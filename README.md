A basic C++ raytracer I wrote for a Computer Graphics course in 2011.
Most of the code is based on Computer Graphics Using OpenGL by Francis S Hill.
Check doc/readme.pdf for more information.
