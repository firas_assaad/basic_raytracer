#include "camera.h"

Camera::Camera(const Vector& e, const Vector& an, const Vector& au, const Vector& av,
        float nr, float g, float s, int rows, int cols, int aa)
        : angle(g), N(nr), aspect(s), numRows(rows), numCols(cols),
        eye(e), n(an), u(au), v(av), antialias(aa) {
    H = N * tan(angle/2);
    W = H * aspect;
}

Ray Camera::rcRay(int r, int c) const {
	Ray ray;
	ray.origin = eye;
	float oneOverNumCols = 1.0f / numCols;
	float oneOverNumRows = 1.0f / numRows;
	ray.direction = -N * n +
		W * (2 * c * oneOverNumCols - 1) * u +
		H * (2 * r * oneOverNumRows - 1) * v;
	return ray;
}

Color Camera::antiAlias(const Scene& scene, int row, int col, int blockSize, float aa) const {
    std::vector<Ray> rays;
    if (antialias >= 2) {
        rays.push_back(rcRay(row, col + blockSize));
    }
    if (antialias >= 4) {
        rays.push_back(rcRay(row + blockSize, col));
        rays.push_back(rcRay(row + blockSize, col + blockSize));
    }
    if (antialias >= 8) {
        rays.push_back(rcRay(row, col + blockSize * 2));
        rays.push_back(rcRay(row, col + blockSize * 3));
        rays.push_back(rcRay(row + blockSize, col + blockSize * 2));
        rays.push_back(rcRay(row + blockSize, col + blockSize * 3));
    }
    Color color;
    for(unsigned int i = 0; i < rays.size(); ++i) {
        color = color + scene.shade(rays[i]) / aa;
    }
    return color;
}

void Camera::rayTrace(const Scene& scene, int blockSize) const {
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(0, numCols, 0, numRows);
    glDisable(GL_LIGHTING);

	float aa  = static_cast<float>(antialias);
    for (int row = 0; row < numRows; row += blockSize) {
        for (int col = 0; col < numCols; col += blockSize) {
            Ray ray = rcRay(row, col);
            Color color = scene.shade(ray) / aa;
            if (antialias > 1) {
                color = color + antiAlias(scene, row, col, blockSize, aa);
            }
            glColor3f(color.r, color.g, color.b);
            glRecti(col, row, col + blockSize, row + blockSize);
        }
    }
}

