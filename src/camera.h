#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glut.h>
#include <iostream>
#include "math3d.h"
#include "scene.h"


class Camera {
private:
	float angle;
    float N;
    float aspect;
    float W;
    float H;
    int numRows;
    int numCols;
    Vector eye;
    Vector n, u, v;
    int antialias;
public:
    Camera(const Vector& e, const Vector& an, const Vector& au, const Vector& av,
           float nr, float g, float s, int rows, int cols, int aa = 1);
    Ray rcRay(int r, int c) const;
    Color antiAlias(const Scene& scene, int row, int col, int blockSize, float aa) const;
    void rayTrace(const Scene& scene, int blockSize) const;
};

#endif
