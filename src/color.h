#ifndef COLOR_H
#define COLOR_H

#include <cmath>
#include <algorithm>

class Color {
public:
    float r, g, b;
    Color() : r(0), g(0), b(0) {}
    Color(float red, float green, float blue){
        set(red, green, blue);
    }
    void set(float red, float green, float blue) {
        r = std::min(1.0f, std::max(0.0f, red));
        g = std::min(1.0f, std::max(0.0f, green));
        b = std::min(1.0f, std::max(0.0f, blue));
    }
    Color operator+(const Color& other) const {
        Color result;
        result.r = std::min(1.0f, std::max(0.0f, r + other.r));
        result.g = std::min(1.0f, std::max(0.0f, g + other.g));
        result.b = std::min(1.0f, std::max(0.0f, b + other.b));
        return result;
    }
    Color operator-(const Color& other) const {
        return *this + -other;
    }
    Color operator-() const {
        return Color(-r, -g, -b);
    }
    Color operator*(const Color& other) const {
        Color result;
        result.r = std::min(1.0f, std::max(0.0f, r * other.r));
        result.g = std::min(1.0f, std::max(0.0f, g * other.g));
        result.b = std::min(1.0f, std::max(0.0f, b * other.b));
        return result;
    }
    bool operator==(const Color& other) const {
        float epsilon = 0.001f;
        return std::fabs(r - other.r) < epsilon &&
            std::fabs(g - other.g) < epsilon &&
            std::fabs(b - other.b) < epsilon;
    }
    bool operator!=(const Color& other) const {
        return !operator==(other);
    }

    friend Color operator*(const Color& c, float k);
    friend Color operator*(float k, const Color& c);
    friend Color operator/(const Color& c, float k);
    friend Color operator/(float k, const Color& c);
};

inline Color operator*(const Color& c, float k) {
    return Color(c.r * k, c.g * k, c.b * k);
}
inline Color operator*(float k, const Color& c) {
    return c * k;
}
inline Color operator/(const Color& c, float k) {
    float oneOverK = 1 / k;
    return c * oneOverK;
}
inline Color operator/(float k, const Color& c) {
    return c / k;
}

#endif
