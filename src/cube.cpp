#include "shapes.h"

/* For a point to be inside a cube it must lie inside all of the
 * inner half-planes of each side of the cube. A candidite interval
 * ci = [tIn, tOut] starts infinitely big and is resized as intersection
 * with each side is tested. Plane intersection is calculated as
 * t = numer / denom where
 * >> numer = normal . (B - ray.origin)
 * >> denom = normal . ray.direction
 * these values are based on substituting the ray into the plane equation
 * >> F(P) = normal . (P - B)
 * where B is a point on the plane. The planes are named as follows:
 * top (0), bottom (1), right (2), left (3), front (4), back (5)
 * Since these planes are known we can easily precalculate numer and denom.
 * If denom < 0, ray passes inside, and if denom > 0, ray passes outside,
 * and if denom = 0 then if numer > 0 ray is wholly inside otherwise it
 * is wholly outside.
 */
std::vector<HitRecord> Cube::intersect(const Ray& ray) {
    std::vector<HitRecord> hits;
    Ray genRay;
    genRay.origin =  matrixNode.inverse * ray.origin;
    genRay.direction =  matrixNode.inverse * ray.direction;
    float tIn = std::numeric_limits<float>::min();
    float tOut = std::numeric_limits<float>::max();
    int entrySurface, exitSurface;
    float numer[6], denom[6];
    // Ray(t) = s + ct
    float cx = genRay.direction.x;
    float cy = genRay.direction.y;
    float cz = genRay.direction.z;
    float sx = genRay.origin.x;
    float sy = genRay.origin.y;
    float sz = genRay.origin.z;
    // set numerator and denominator for each plane
    numer[0] = 1.0f - sy;
    numer[1] = 1.0f + sy;
    numer[2] = 1.0f - sx;
    numer[3] = 1.0f + sx;
    numer[4] = 1.0f - sz;
    numer[5] = 1.0f + sz;
    denom[0] = cy;
    denom[1] = -cy;
    denom[2] = cx;
    denom[3] = -cx;
    denom[4] = cz;
    denom[5] = -cz;
    // for each surface
    for (int i = 0; i < 6; i++) {
        // If ray is parallel
        if (std::abs(denom[i]) < EPSILON) {
            // If ray is wholly outside, no intersections
            if (numer[i] < 0)
                return hits;
        }
        else {
            float t = numer[i] / denom[i];
            if (denom[i] > 0) {
                // exiting
                if (t < tOut) {
                    // an earlier exit
                    tOut = t;
                    exitSurface = i;
                }
            } else {
                // entering
                if (t > tIn) {
                    // a later entrance
                    tIn = t;
                    entrySurface = i;
                }
            }
        }
        if (tIn >= tOut)
            return hits;
    }
    // Is the first hit in front of the eye?
    if (tIn > EPSILON) {
        HitRecord record;
        record.time = tIn;
        record.surface = entrySurface;
        record.entering = true;
        record.object = this;
        record.point = genRay.position(tIn);
        record.surfaceNormal = normalForFace(entrySurface);
        hits.push_back(record);
    }
    if (tOut > EPSILON) {
        HitRecord record;
        record.time = tOut;
        record.surface = exitSurface;
        record.entering = false;
        record.object = this;
        record.point = genRay.position(tOut);
        record.surfaceNormal = normalForFace(exitSurface);
        hits.push_back(record);
    }
    return hits;
}
Vector Cube::normalForFace(int faceNumber) {
    Vector result;
    switch (faceNumber) {
        case 0: // top
            result.set(0.0f, 1.0f, 0.0f, 0.0f);
            break;
        case 1: // bottom
            result.set(0.0f, -1.0f, 0.0f, 0.0f);
            break;
        case 2: // right
            result.set(1.0f, 0.0f, 0.0f, 0.0f);
            break;
        case 3: // left
            result.set(-1.0f, 0.0f, 0.0f, 0.0f);
            break;
        case 4: // front
            result.set(0.0f, 0.0f, 1.0f, 0.0f);
            break;
        case 5: // back
            result.set(0.0f, 0.0f, -1.0f, 0.0f);
            break;
    }
    return result;
}