#ifndef HIT_RECORD_H
#define HIT_RECORD_H

class Object;

class HitRecord {
public:
    float time;
    Object* object;
    bool entering;
    int surface;
    Vector point;
    Vector surfaceNormal;
};

#endif
