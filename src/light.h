#ifndef LIGHT_H
#define LIGHT_H

#include "math3d.h"
#include "color.h"

class Light {
public:
    Vector position;
    Color  diffuse;
    Color ambient;
    Color specular;
};

#endif
