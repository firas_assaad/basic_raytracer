#include <iostream>
#include <GL/glut.h>
#include "math3d.h"
#include "camera.h"
#include "shapes.h"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
float oa = 0;
float la = 0;
int blockSize = 1;
int antialias = 1;
void (*setupScene)();
Camera* camera;
Scene* scene;

void objectTest() {

    Sphere* sphere = new Sphere;
    scene->getMatrixStack().push();
    scene->getMatrixStack().scale(0.5f, 0.5f, 0.5f);
    sphere->material.ambient = Color(0.2f, 0.2f, 0.2f);
    sphere->material.diffuse = Color(0.8f, 0.8f, 0.8f);
    sphere->material.specular = Color(1.0f, 1.0f, 1.0f);
    sphere->material.specularExponent = 70;
    scene->addObject(sphere);
    scene->getMatrixStack().pop();

    scene->getMatrixStack().push();
    TaperedCylinder* cylinder = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.5f, 0.5f, 0.5f);
    scene->getMatrixStack().translate(Vector(2, 0, 0));
    cylinder->material.ambient = Color(0.25f, 0.19f, 0.07f);
    cylinder->material.diffuse = Color(0.75f, 0.6f, 0.23f);
    cylinder->material.specular = Color(0.63f, 0.56f, 0.36f);
    cylinder->material.specularExponent = 100;
    scene->addObject(cylinder);
    scene->getMatrixStack().pop();

    scene->getMatrixStack().push();
    Square* square = new Square;
    scene->getMatrixStack().scale(2.0f, 2.0f, 2.0f);
    scene->getMatrixStack().rotate(45, Vector(0, 0, 1));
    square->material.ambient = Color(0.2f, 0.2f, 0.2f);
    square->material.diffuse = Color(0.2f, 1.0f, 0.2f);
    square->material.specular = Color(1.0f, 1.0f, 1.0f);
    square->material.specularExponent = 100;
    scene->addObject(square);
    scene->getMatrixStack().pop();

    scene->getMatrixStack().push();
    Cube* cube = new Cube;
    scene->getMatrixStack().scale(0.5f, 0.5f, 0.5f);
    scene->getMatrixStack().rotate(0, Vector(1, 0, 0));
    scene->getMatrixStack().translate(Vector(-2, 0, 0));
    cube->material.ambient = Color(0.2f, 0.2f, 0.2f);
    cube->material.diffuse = Color(0.2f, 0.2f, 1.0f);
    cube->material.specular = Color(1.0f, 1.0f, 1.0f);
    cube->material.specularExponent = 80;
    scene->addObject(cube);
    scene->getMatrixStack().pop();

    Light l;
    l.position.set(0, 0, 5, 1);
    l.ambient.set(0.2f, 0.2f, 0.2f);
    l.diffuse.set(1.0f, 0.8f, 0.8f);
    l.specular.set(1.0f, 1.0f, 1.0f);
	scene->addLight(l);
}

void libTower() {
    // Ground
    scene->getMatrixStack().push();
    Square* ground = new Square();
    scene->getMatrixStack().scale(20.0f, 1.0f, 20.0f);
    scene->getMatrixStack().rotate(-90, Vector(1.0f, 0.0f, 0.0f));
    ground->material.diffuse.set(0.4f, 1.0f, 0.5f);
    scene->addObject(ground);
    scene->getMatrixStack().pop();

    // Base
    scene->getMatrixStack().push();
    Cube* base = new Cube();
    scene->getMatrixStack().scale(0.8f, 0.1f, 0.8f);
    scene->getMatrixStack().translate(Vector(0.0f, 1.0f, 0.0f));
    base->material.ambient.set(0.3f, 0.3f, 0.3f);
    base->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(base);
    scene->getMatrixStack().pop();

    // Cone
    scene->getMatrixStack().push();
    TaperedCylinder* cone = new TaperedCylinder(0.0f);
    scene->getMatrixStack().scale(0.25f, 5.0f, 0.25f);
    scene->getMatrixStack().rotate(-90, Vector(1.0f, 0.0f, 0.0f));
    cone->material.ambient.set(0.3f, 0.3f, 0.3f);
    cone->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(cone);
    scene->getMatrixStack().pop();

    // Saucer bottom
    scene->getMatrixStack().push();
    TaperedCylinder* saucerBottom = new TaperedCylinder(0.5f);
    scene->getMatrixStack().scale(0.4f, 0.15f, 0.4f);
    scene->getMatrixStack().translate(Vector(0.0f, 12.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    saucerBottom->material.ambient.set(0.3f, 0.3f, 0.3f);
    saucerBottom->material.diffuse.set(0.1f, 0.3f, 0.2f);
    saucerBottom->material.specular.set(0.2f, 0.5f, 0.3f);
    saucerBottom->material.specularExponent = 80;
    scene->addObject(saucerBottom);
    scene->getMatrixStack().pop();

    // Saucer middle 1
    scene->getMatrixStack().push();
    TaperedCylinder* saucerMid1 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.4f, 0.1f, 0.4f);
    scene->getMatrixStack().translate(Vector(0.0f, 19.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    saucerMid1->material.ambient.set(0.3f, 0.3f, 0.3f);
    saucerMid1->material.diffuse.set(0.1f, 0.3f, 0.2f);
    saucerMid1->material.specular.set(0.2f, 0.5f, 0.3f);
    saucerMid1->material.specularExponent = 80;
    scene->addObject(saucerMid1);
    scene->getMatrixStack().pop();

    // Saucer middle 2
    scene->getMatrixStack().push();
    TaperedCylinder* saucerMid2 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.5f, 0.1f, 0.5f);
    scene->getMatrixStack().translate(Vector(0.0f, 20.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    saucerMid2->material.ambient.set(0.3f, 0.3f, 0.3f);
    saucerMid2->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(saucerMid2);
    scene->getMatrixStack().pop();

    // Saucer top
    scene->getMatrixStack().push();
    TaperedCylinder* saucerTop = new TaperedCylinder(0.3f);
    scene->getMatrixStack().scale(0.51f, 0.15f, 0.51f);
    scene->getMatrixStack().translate(Vector(0.0f, 13.3f, 0.0f));
    scene->getMatrixStack().rotate(-90, Vector(1.0f, 0.0f, 0.0f));
    saucerTop->material.ambient.set(0.3f, 0.3f, 0.3f);
    saucerTop->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(saucerTop);
    scene->getMatrixStack().pop();

    // Cylinder bottom
    scene->getMatrixStack().push();
    TaperedCylinder* cylinderBottom = new TaperedCylinder(0.5f);
    scene->getMatrixStack().scale(0.28f, 0.15f, 0.28f);
    scene->getMatrixStack().translate(Vector(0.0f, 18.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    cylinderBottom->material.ambient.set(0.3f, 0.3f, 0.3f);
    cylinderBottom->material.diffuse.set(0.3f, 0.2f, 0.1f);
    cylinderBottom->material.specular.set(0.5f, 0.3f, 0.2f);
    cylinderBottom->material.specularExponent = 80;
    scene->addObject(cylinderBottom);
    scene->getMatrixStack().pop();
    // Cylinder
    scene->getMatrixStack().push();
    TaperedCylinder* cylinder = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.28f, 0.45f, 0.28f);
    scene->getMatrixStack().translate(Vector(0.0f, 7.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    cylinder->material.ambient.set(0.3f, 0.3f, 0.3f);
    cylinder->material.diffuse.set(0.3f, 0.2f, 0.1f);
    cylinder->material.specular.set(0.5f, 0.3f, 0.2f);
    cylinder->material.specularExponent = 80;
    scene->addObject(cylinder);
    scene->getMatrixStack().pop();
    // Cylinder top
    scene->getMatrixStack().push();
    TaperedCylinder* cylinderTop = new TaperedCylinder(0.5f);
    scene->getMatrixStack().scale(0.28f, 0.1f, 0.28f);
    scene->getMatrixStack().translate(Vector(0.0f, 31.51f, 0.0f));
    scene->getMatrixStack().rotate(-90, Vector(1.0f, 0.0f, 0.0f));
    cylinderTop->material.ambient.set(0.3f, 0.3f, 0.3f);
    cylinderTop->material.diffuse.set(0.3f, 0.2f, 0.1f);
    cylinderTop->material.specular.set(0.5f, 0.3f, 0.2f);
    cylinderTop->material.specularExponent = 80;
    scene->addObject(cylinderTop);
    scene->getMatrixStack().pop();
    // Big disc 1
    scene->getMatrixStack().push();
    TaperedCylinder* bigDisc1 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.37f, 0.01f, 0.37f);
    scene->getMatrixStack().translate(Vector(0.0f, 270.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    bigDisc1->material.ambient.set(0.3f, 0.3f, 0.3f);
    bigDisc1->material.diffuse.set(0.3f, 0.2f, 0.1f);
    bigDisc1->material.specular.set(0.5f, 0.3f, 0.2f);
    bigDisc1->material.specularExponent = 80;
    scene->addObject(bigDisc1);
    scene->getMatrixStack().pop();
    // Big disc 2
    scene->getMatrixStack().push();
    TaperedCylinder* bigDisc2 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.37f, 0.01f, 0.37f);
    scene->getMatrixStack().translate(Vector(0.0f, 278.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    bigDisc2->material.ambient.set(0.3f, 0.3f, 0.3f);
    bigDisc2->material.diffuse.set(0.3f, 0.2f, 0.1f);
    bigDisc2->material.specular.set(0.5f, 0.3f, 0.2f);
    bigDisc2->material.specularExponent = 80;
    scene->addObject(bigDisc2);
    scene->getMatrixStack().pop();
    // Big disc 3
    scene->getMatrixStack().push();
    TaperedCylinder* bigDisc3 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.37f, 0.01f, 0.37f);
    scene->getMatrixStack().translate(Vector(0.0f, 286.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    bigDisc3->material.ambient.set(0.3f, 0.3f, 0.3f);
    bigDisc3->material.diffuse.set(0.3f, 0.2f, 0.1f);
    bigDisc3->material.specular.set(0.5f, 0.3f, 0.2f);
    bigDisc3->material.specularExponent = 80;
    scene->addObject(bigDisc3);
    scene->getMatrixStack().pop();
    // Cylinder tube
    scene->getMatrixStack().push();
    TaperedCylinder* cylinderTube = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.15f, 0.2f, 0.15f);
    scene->getMatrixStack().translate(Vector(0.0f, 17.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    cylinderTube->material.ambient.set(0.3f, 0.3f, 0.3f);
    cylinderTube->material.diffuse.set(0.3f, 0.2f, 0.1f);
    cylinderTube->material.specular.set(0.5f, 0.3f, 0.2f);
    cylinderTube->material.specularExponent = 80;
    scene->addObject(cylinderTube);
    scene->getMatrixStack().pop();
    // Small disc 1
    scene->getMatrixStack().push();
    TaperedCylinder* smallDisc1 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.20f, 0.02f, 0.20f);
    scene->getMatrixStack().translate(Vector(0.0f, 172.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    smallDisc1->material.ambient.set(0.3f, 0.3f, 0.3f);
    smallDisc1->material.diffuse.set(0.3f, 0.2f, 0.1f);
    smallDisc1->material.specular.set(0.5f, 0.3f, 0.2f);
    smallDisc1->material.specularExponent = 80;
    scene->addObject(smallDisc1);
    scene->getMatrixStack().pop();
    // Small disc 2
    scene->getMatrixStack().push();
    TaperedCylinder* smallDisc2 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.20f, 0.02f, 0.20f);
    scene->getMatrixStack().translate(Vector(0.0f, 180.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    smallDisc2->material.ambient.set(0.3f, 0.3f, 0.3f);
    smallDisc2->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(smallDisc2);
    scene->getMatrixStack().pop();
    // Small disc 3
    scene->getMatrixStack().push();
    TaperedCylinder* smallDisc3 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.20f, 0.02f, 0.20f);
    scene->getMatrixStack().translate(Vector(0.0f, 188.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    smallDisc3->material.ambient.set(0.3f, 0.3f, 0.3f);
    smallDisc3->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(smallDisc3);
    scene->getMatrixStack().pop();
    // Small disc 4
    scene->getMatrixStack().push();
    TaperedCylinder* smallDisc4 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.1f, 0.02f, 0.1f);
    scene->getMatrixStack().translate(Vector(0.0f, 205.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    smallDisc4->material.ambient.set(0.3f, 0.3f, 0.3f);
    smallDisc4->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(smallDisc4);
    scene->getMatrixStack().pop();
    // Small disc 5
    scene->getMatrixStack().push();
    TaperedCylinder* smallDisc5 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.08f, 0.02f, 0.08f);
    scene->getMatrixStack().translate(Vector(0.0f, 220.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    smallDisc5->material.ambient.set(0.3f, 0.3f, 0.3f);
    smallDisc5->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(smallDisc5);
    scene->getMatrixStack().pop();
    // Small disc 6
    scene->getMatrixStack().push();
    TaperedCylinder* smallDisc6 = new TaperedCylinder(1.0f);
    scene->getMatrixStack().scale(0.06f, 0.02f, 0.06f);
    scene->getMatrixStack().translate(Vector(0.0f, 235.0f, 0.0f));
    scene->getMatrixStack().rotate(90, Vector(1.0f, 0.0f, 0.0f));
    smallDisc6->material.ambient.set(0.3f, 0.3f, 0.3f);
    smallDisc6->material.diffuse.set(0.7f, 0.7f, 0.7f);
    scene->addObject(smallDisc6);
    scene->getMatrixStack().pop();
}

void lights() {
    // Lights
    Matrix top = scene->getMatrixStack().top().matrix;
    Light l;
    l.position = top  * Vector(2, 6, 6, 1);
    l.ambient.set(0.2f, 0.2f, 0.2f);
    l.diffuse.set(0.8f, 0.8f, 0.8f);
    l.specular.set(1.0f, 1.0f, 1.0f);
    scene->addLight(l);
    Light l2;
    l2.position = top * Vector(-2, 1, 6, 1);
    l2.ambient.set(0.2f, 0.2f, 0.2f);
    l2.diffuse.set(0.5f, 0.5f, 0.5f);
    l2.specular.set(1.0f, 1.0f, 1.0f);
    scene->addLight(l2);
}

void init() {
    glViewport(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    Vector eye(0, 2.3f, 5, 1);
    Vector n(0, 0, 1);
    Vector u(1, 0, 0);
    Vector v(0, 1, 0);
    float N = 2;
    float angle = static_cast<float>(M_PI) / 3;
    float aspect = (float)SCREEN_WIDTH / SCREEN_HEIGHT;
    int rows = SCREEN_WIDTH;
    int cols = SCREEN_HEIGHT;
    camera = new Camera(eye, n, u, v, N, angle, aspect, rows, cols, antialias);
    scene = new Scene;
	setupScene = libTower;
    setupScene();
    lights();

}

void display() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    camera->rayTrace(*scene, blockSize);
    glutSwapBuffers();
}

void idle() {
    //glutPostRedisplay();
}

void special(int key, int x, int y) {
    if (key == GLUT_KEY_UP || key == GLUT_KEY_DOWN) {
        if (key == GLUT_KEY_UP) {
            la -= 1;
        }
        if (key == GLUT_KEY_DOWN) {
            la += 1;
        }
        scene->clearLights();
        scene->getMatrixStack().identity();
        scene->getMatrixStack().rotate(la, Vector(0.0, 1.0, 0.0));
        lights();
    }
    if (key == GLUT_KEY_LEFT || key == GLUT_KEY_RIGHT) {
        if (key == GLUT_KEY_LEFT) {
            oa -= 1;
        }
        if (key == GLUT_KEY_RIGHT) {
            oa += 1;
        }
        scene->clearObjects();
        scene->getMatrixStack().identity();
        scene->getMatrixStack().rotate(oa, Vector(0.0, 1.0, 0.0));
		setupScene();
    }
    glutPostRedisplay();
}

void key(unsigned char key, int x, int y) {
    if (key == 'w') {
        blockSize++;
    }
    if (key == 's') {
        if (blockSize > 1)
            blockSize--;
    }
    glutPostRedisplay();
}

int main(int argc, char* argv[]) {
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(SCREEN_WIDTH,SCREEN_HEIGHT);
    glutInitWindowPosition(100, 100);
    glutCreateWindow("Raytracing");
    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(key);
    glutSpecialFunc(special);
    init();
    glutMainLoop();
    delete camera;
    delete scene;
}
