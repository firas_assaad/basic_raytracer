#ifndef MATERIAL_H
#define MATERIAL_H

#include "color.h"

class Material {
public:
    Color ambient;
    Color diffuse;
    Color specular;
    float specularExponent;
};

#endif
