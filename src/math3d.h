#ifndef MATH3D_H
#define MATH3D_H

#define _USE_MATH_DEFINES
#include <math.h>
#include "vector.h"
#include "matrix.h"
#include "ray.h"

const float EPSILON = 0.00001f;

#endif
