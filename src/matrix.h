#ifndef MATRIX_H
#define MATRIX_H

#include "vector.h"

class Matrix {
public:
    float m11, m12, m13, m14,
        m21, m22, m23, m24,
        m31, m32, m33, m34;

    Matrix() : m11(1), m12(0), m13(0), m14(0),
        m21(0), m22(1), m23(0), m24(0),
        m31(0), m32(0), m33(1), m34(0)
    {
    }
    Matrix(float a[16]) {
        set(a);
    }
    Matrix(Vector c1, Vector c2, Vector c3, Vector c4) {
        float a[16] = {
            c1.x, c1.y, c1.z, c1.w,
            c2.x, c2.y, c2.z, c2.w,
            c3.x, c3.y, c3.z, c3.w,
            c4.x, c4.y, c4.z, c4.w
        };
        set(a);
    }
    void setIdentity() {
        float a[16] = {
            1, 0, 0,0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        };
        set(a);
    }
    void set(float a[16]) {
        m11 = a[0];
        m21 = a[1];
        m31 = a[2];
        // m41 = 0
        m12 = a[4];
        m22 = a[5];
        m32 = a[6];
        // m42 = 0
        m13 = a[8];
        m23 = a[9];
        m33 = a[10];
        // m43 = 0
        m14 = a[12];
        m24 = a[13];
        m34 = a[14];
        // m44 = 1
    }
    Matrix operator+(const Matrix& other) const {
        Matrix result;
        result.m11 = m11 + other.m11;
        result.m21 = m21 + other.m21;
        result.m31 = m31 + other.m31;
        result.m12 = m12 + other.m12;
        result.m22 = m22 + other.m22;
        result.m32 = m32 + other.m32;
        result.m13 = m13 + other.m13;
        result.m23 = m23 + other.m23;
        result.m33 = m33 + other.m33;
        result.m14 = m14 + other.m14;
        result.m24 = m24 + other.m24;
        result.m34 = m34 + other.m34;
        return result;
    }
    Matrix operator-() const {
        Matrix result;
        result.m11 = -m11;
        result.m21 = -m21;
        result.m31 = -m31;
        result.m12 = -m12;
        result.m22 = -m22;
        result.m32 = -m32;
        result.m13 = -m13;
        result.m23 = -m23;
        result.m33 = -m33;
        result.m14 = -m14;
        result.m24 = -m24;
        result.m34 = -m34;
        return result;
    }
    Matrix operator-(const Matrix& other) const {
        return *this + -other;
    }
    friend Matrix operator*(const Matrix& m, float k);
    friend Matrix operator*(float k, const Matrix& m);
    friend Matrix operator/(const Matrix& m, float k);
    friend Matrix operator/(float k, const Matrix& m);

    Matrix operator*(const Matrix& other) const {
        Matrix result;
        result.m11 = m11 * other.m11 + m12 * other.m21 + m13 * other.m31;
        result.m21 = m21 * other.m11 + m22 * other.m21 + m23 * other.m31;
        result.m31 = m31 * other.m11 + m32 * other.m21 + m33 * other.m31;
        result.m12 = m11 * other.m12 + m12 * other.m22 + m13 * other.m32;
        result.m22 = m21 * other.m12 + m22 * other.m22 + m23 * other.m32;
        result.m32 = m31 * other.m12 + m32 * other.m22 + m33 * other.m32;
        result.m13 = m11 * other.m13 + m12 * other.m23 + m13 * other.m33;
        result.m23 = m21 * other.m13 + m22 * other.m23 + m23 * other.m33;
        result.m33 = m31 * other.m13 + m32 * other.m23 + m33 * other.m33;
        result.m14 = m11 * other.m14 + m12 * other.m24 + m13 * other.m34 + m14;
        result.m24 = m21 * other.m14 + m22 * other.m24 + m23 * other.m34 + m24;
        result.m34 = m31 * other.m14 + m32 * other.m24 + m33 * other.m34 + m34;
        return result;
    }
    Vector operator*(const Vector& v) const {
        Vector result;
        result.x = m11 * v.x + m12 * v.y + m13 * v.z + m14 * v.w;
        result.y = m21 * v.x + m22 * v.y + m23 * v.z + m24 * v.w;
        result.z = m31 * v.x + m32 * v.y + m33 * v.z + m34 * v.w;
        result.w = v.w;
        return result;
    }
    Matrix transpose() {
        // ignores translation part!
        Matrix result;
        result.m11 = m11;
        result.m21 = m12;
        result.m31 = m13;
        result.m12 = m21;
        result.m22 = m22;
        result.m32 = m23;
        result.m13 = m31;
        result.m23 = m32;
        result.m33 = m33;
        result.m14 = 0;
        result.m24 = 0;
        result.m34 = 0;
        return result;
    }
};

inline Matrix operator*(const Matrix& m, float k) {
    Matrix result;
    result.m11 = k * m.m11;
    result.m21 = k * m.m21;
    result.m31 = k * m.m31;
    result.m12 = k * m.m12;
    result.m22 = k * m.m22;
    result.m32 = k * m.m32;
    result.m13 = k * m.m13;
    result.m23 = k * m.m23;
    result.m33 = k * m.m33;
    result.m14 = k * m.m14;
    result.m24 = k * m.m24;
    result.m34 = k * m.m34;
    return result;
}
inline Matrix operator*(float k, const Matrix& m) {
    return m * k;
}
inline Matrix operator/(const Matrix& m, float k) {
    return m * (1.0f / k);
}
inline Matrix operator/(float k, const Matrix& m) {
    return m / k;
}

inline std::ostream& operator <<(std::ostream& o, const Matrix& m)
{
    std::streamsize old = o.precision(3);
    o << m.m11 << "\t" << m.m12 << "\t" << m.m13 << "\t" << m.m14 << std::endl;
    o << m.m21 << "\t" << m.m22 << "\t" << m.m23 << "\t" << m.m24 << std::endl;
    o << m.m31 << "\t" << m.m32 << "\t" << m.m33 << "\t" << m.m34 << std::endl;
    o << 0 << "\t" << 0 << "\t" << 0 << "\t" << 1 << std::endl;
    o.precision(old);
    return o;
}

#endif