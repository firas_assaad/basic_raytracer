#include "matrix_stack.h"

void MatrixStack::translate(const Vector& d) {
    MatrixStackNode node;
    node.matrix.m14 = d.x;
    node.matrix.m24 = d.y;
    node.matrix.m34 = d.z;
    node.inverse.m14 = -d.x;
    node.inverse.m24 = -d.y;
    node.inverse.m34 = -d.z;

    MatrixStackNode top = this->top();
    top.matrix = top.matrix * node.matrix;
    top.inverse = node.inverse * top.inverse;
    setTop(top);
}

void MatrixStack::scale(float sx, float sy, float sz) {
    MatrixStackNode node;
    node.matrix.m11 = sx;
    node.matrix.m22 = sy;
    node.matrix.m33 = sz;
    node.inverse.m11 = 1.0f / sx;
    node.inverse.m22 = 1.0f / sy;
    node.inverse.m33 = 1.0f / sz;

    MatrixStackNode top = this->top();
    top.matrix = top.matrix * node.matrix;
    top.inverse = node.inverse * top.inverse;
    setTop(top);
}

void MatrixStack::rotate(float angle, const Vector& u) {
    MatrixStackNode node;
    Vector nu = normalize(u);
    float radians = angle * static_cast<float>(M_PI) / 180;
    float c = std::cos(radians);
    float s = std::sin(radians);
    float mc = 1.0f - c;

    node.matrix.m11 = c + mc * u.x * u.x;
    node.matrix.m21 = mc * u.x * u.y + s * u.z;
    node.matrix.m31 = mc * u.x * u.z - s * u.y;

    node.matrix.m12 = mc * u.y * u.x - s * u.z;
    node.matrix.m22 = c + mc * u.y * u.y;
    node.matrix.m32 = mc * u.y * u.z + s * u.x;

    node.matrix.m13 = mc * u.z * u.x + s * u.y;
    node.matrix.m23 = mc * u.z * u.y - s * u.x;
    node.matrix.m33 = c + mc * u.z * u.z;

    node.inverse.m11 = c + mc * u.x * u.x;
    node.inverse.m21 = mc * u.x * u.y - s * u.z;
    node.inverse.m31 = mc * u.x * u.z + s * u.y;

    node.inverse.m12 = mc * u.y * u.x + s * u.z;
    node.inverse.m22 = c + mc * u.y * u.y;
    node.inverse.m32 = mc * u.y * u.z - s * u.x;

    node.inverse.m13 = mc * u.z * u.x - s * u.y;
    node.inverse.m23 = mc * u.z * u.y + s * u.x;
    node.inverse.m33 = c + mc * u.z * u.z;

    MatrixStackNode top = this->top();
    top.matrix = top.matrix * node.matrix;
    top.inverse = node.inverse * top.inverse;
    setTop(top);
}