#ifndef MATRIX_STACK_H
#define MATRIX_STACK_H

#include <stack>
#include "math3d.h"

class MatrixStackNode {
public:
    Matrix matrix;
    Matrix inverse;
};

class MatrixStack {
private:
    std::stack<MatrixStackNode> matrices;
public:
    MatrixStack() {
        // push initial matrix
        matrices.push(MatrixStackNode());
    }
    void push() {
        matrices.push(top());
    }
    void pop() {
        matrices.pop();
    }
    MatrixStackNode top() const {
        return matrices.top();
    }
    bool empty() const {
        return matrices.empty();
    }
    void identity() {
        setTop(MatrixStackNode());
    }
    void setTop(const MatrixStackNode& m) {
        if (empty())
            return;
        matrices.pop();
        matrices.push(m);
    }
    void translate(const Vector& d);
    void scale(float sx, float sy, float sz);
    void rotate(float angle, const Vector& u);
};

#endif
