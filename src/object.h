#ifndef OBJECT_H
#define OBJECT_H

#include <vector>
#include "matrix_stack.h"
#include "hit_record.h"
#include "material.h"

class Object {
public:
    Material material;
    MatrixStackNode matrixNode;
    virtual std::vector<HitRecord> intersect(const Ray& ray) = 0;
};

#endif
