#ifndef RAY_H
#define RAY_H


#include "vector.h"

class Ray {
public:
    Vector direction;
    Vector origin;
    int recursionLevel;
    Ray() : recursionLevel(0) {};
    Ray(const Vector& d, const Vector& o)
        : direction(d), origin(o), recursionLevel(0) {}
    Vector position(float t) const {
        return origin + direction * t;
    }
};

inline std::ostream& operator <<(std::ostream& o, const Ray& r)
{
    o << "Point: " << r.origin << " - Direction: " << r.direction;
    return o;
}

#endif