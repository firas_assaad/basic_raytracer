#include "scene.h"

Color Scene::shade(const Ray& ray) const {
    std::vector<HitRecord> hits = getFirstHit(ray);
    if (hits.empty()) {
        return background;
    }
    return calculateColor(ray, hits[0]);
}

void Scene::addObject(Object* object) {
    object->matrixNode = matrixStack.top();
    objects.push_back(object);
}

void Scene::addLight(const Light& light) {
	lights.push_back(light);
}

Color Scene::calculateColor(const Ray& ray, const HitRecord& record) const {
    Color result;
    Matrix inverse = record.object->matrixNode.inverse;
    Vector normal = normalize(inverse.transpose() * record.surfaceNormal);
    Vector hitPoint = ray.position(record.time);
    Vector v = -ray.direction; // direction to the viewer
    v = normalize(v);
    Material material = record.object->material;
    result = result + globalAmbient;
    Ray shadowRay;
    shadowRay.recursionLevel = 1;
    shadowRay.origin = hitPoint - EPSILON * ray.direction;
    for (unsigned int i = 0; i < lights.size(); i++) {
        Light light = lights[i];
        Color ambient = light.ambient * material.ambient;
        result = result + ambient;
        shadowRay.direction = light.position - shadowRay.origin;
        if (inShadow(shadowRay))
            continue;
        Vector s = light.position - hitPoint;
        s = normalize(s);
        float nDotS = normal.dot(s);
        if (nDotS > 0.0) { // facing the light
            Color diffuse = nDotS * material.diffuse * light.diffuse;
            result = result + diffuse;
        }

        Vector h = v + s; // half-way vector
        h = normalize(h);
        float nDotH = normal.dot(h);
        if (nDotH <= 0)
            continue;   // no specular
        float phong = std::pow(nDotH, material.specularExponent);
        Color specular = phong * material.specular * light.specular;
        result = result + specular;
    }
    return result;
}

bool Scene::inShadow(const Ray& shadowRay) const {
    for (unsigned int i = 0; i < objects.size(); i++) {
        std::vector<HitRecord> hits = objects[i]->intersect(shadowRay);
        for (unsigned int i = 0; i < hits.size();) {
            if (hits[i].time < 0.0f || hits[i].time > 1.0f) {
                hits.erase(hits.begin() + i);
            } else {
                i++;
            }
        }
        if (!hits.empty()) {
            return true;
        }
    }
    return false;
}

std::vector<HitRecord> Scene::getFirstHit(const Ray& ray) const {
    std::vector<HitRecord> bestHits;
    std::vector<HitRecord> currentHits;
    for (unsigned int i = 0; i < objects.size(); i++) {
        currentHits = objects[i]->intersect(ray);
        if (currentHits.empty())
            continue;
        if (bestHits.empty() || currentHits[0].time < bestHits[0].time) {
            bestHits = currentHits;
        }
    }
    return bestHits;
}

void Scene::clearObjects() {
    for (unsigned int i = 0; i < objects.size(); i++) {
        delete objects[i];
    }
    objects.clear();
}
void Scene::clearLights() {
    lights.clear();
}
Scene::~Scene() {
    clearObjects();
    clearLights();
}