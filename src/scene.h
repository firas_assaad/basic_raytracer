#ifndef SCENE_H
#define SCENE_H

#include <vector>
#include <iostream>
#include "math3d.h"
#include "matrix_stack.h"
#include "object.h"
#include "light.h"

class Scene {
private:
	Color background;
    Color globalAmbient;
    MatrixStack matrixStack;
    std::vector<Object*> objects;
    std::vector<Light> lights;
public:
    Scene() : background(0.5f, 0.5f, 0.7f), globalAmbient(0.1f, 0.1f, 0.1f) {}
	MatrixStack& getMatrixStack() { return matrixStack; }
    Color shade(const Ray& ray) const;
    void addObject(Object* object);
	void addLight(const Light& light);
    Color calculateColor(const Ray& ray, const HitRecord& record) const;
    bool inShadow(const Ray& shadowRay) const;
    std::vector<HitRecord> getFirstHit(const Ray& ray) const;
    void clearObjects();
    void clearLights();
    ~Scene();
};

#endif
