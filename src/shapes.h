#ifndef SHAPES_H
#define SHAPES_H

#include <math.h>
#include <iostream>
#include <limits>
#include "object.h"

class Sphere : public Object{
public:
    
    std::vector<HitRecord> intersect(const Ray& ray);
};

class Square : public Object {
public:
    std::vector<HitRecord> intersect(const Ray& ray);
};

class TaperedCylinder : public Object {
private:
	float smallRadius;
public:
    TaperedCylinder(float sm) : smallRadius(sm) {}
    std::vector<HitRecord> intersect(const Ray& ray);
};

class Cube : public Object {
public:
    std::vector<HitRecord> intersect(const Ray& ray);
    Vector normalForFace(int faceNumber);
};

#endif
