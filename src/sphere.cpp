#include "shapes.h"

/* Sphere intersection is solved by substituting the ray's parametric form
 * into the implicit equation of the sphere (F(P) = P^2 - 1) to get
 * At^2 + 2Bt + C = 0 = 0 with:
 * >> A = direction^2
 * >> B = origin . direction
 * >> C = origin^2 - 1
 * and use the discriminant B^2 - AC to solve the resulting equation for
 * the values of t.
 */
std::vector<HitRecord> Sphere::intersect(const Ray& ray) {
    std::vector<HitRecord> hits;
    Ray genRay;
    genRay.origin =  matrixNode.inverse * ray.origin;
    genRay.direction =  matrixNode.inverse * ray.direction;
    float A = genRay.direction.dot(genRay.direction);
    float B = genRay.origin.dot(genRay.direction);
    float C = genRay.origin.dot(genRay.origin) - 1.0f;
    float discrim = B * B - A * C;
    if (discrim < 0.0f) // ray misses
        return hits;
    float discRoot = std::sqrt(discrim);
    float t1 = (-B - discRoot)/A; // first hit
    // Check if hit is in front of the eye
    if (t1 > EPSILON) {
        HitRecord record;
        record.time = t1;
        record.object = this;
        record.entering = true;
        record.surface = 0;
        Vector p = genRay.position(t1);
        record.point = p;
        record.surfaceNormal = p;
        hits.push_back(record);
    }
    float t2 = (-B + discRoot)/A; // second hit
    if (t2 > EPSILON) {
        HitRecord record;
        record.time = t2;
        record.object = this;
        record.entering = false;
        record.surface = 0;
        Vector p = genRay.position(t2);
        record.point = p;
        record.surfaceNormal = p;
        hits.push_back(record);
    }
    return hits;
}