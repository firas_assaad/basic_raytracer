#include "shapes.h"

/* Square intersection is simply intersection with a generic plane at z = 0:
 * >> point.z + direction.z * t = 0
 * >> t = -point.z/direction.z
 * and we limit x and y to the interval [-1.0, 1.0] for a generic square.
 */
std::vector<HitRecord> Square::intersect(const Ray& ray) {
    std::vector<HitRecord> hits;
    Ray genRay;
    genRay.origin =  matrixNode.inverse * ray.origin;
    genRay.direction =  matrixNode.inverse * ray.direction;
    float denom = genRay.direction.z;
    if (std::fabs(denom) < EPSILON) // ray parallel to plane
        return hits;
    float t = -genRay.origin.z / denom; // hit time
    if (t <= 0.0f) return hits; // behind eye
    Vector hitPoint = genRay.position(t);
    // Check if within generic square
    if (hitPoint.x > 1.0f || hitPoint.x < -1.0f) return hits;
    if (hitPoint.y > 1.0f || hitPoint.y < -1.0f) return hits;
    HitRecord record;
    record.time = t;
    record.object = this;
    record.entering = true;
    record.surface = 0;
    record.point = hitPoint;
    record.surfaceNormal = Vector(0.0f, 0.0f, 1.0f);
    hits.push_back(record);
    return hits;
}