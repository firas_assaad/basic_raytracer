#include "shapes.h"

/* Tapered cylinder intersection involves fighting the intersections
 * with 3 different surfaces: the wall (0), the base(1), and the cap(2).
 * The wall and cap are tested as circle shaped planes, while the wall
 * uses an implicit formula of the tapered cylinder:
 * >> F(x, y, z) = x^2 + y^2 - (1 + (s - 1)z)^2 for 0 < z < 1
 * and we substitue the ray parametric form into F to get
 * >> At^2 + 2Bt + C = 0
 * >> A = direction.x^2 + direction.y^2 - d^2
 * >> B = origin.x * direction.x + origin.y * direction.y - F * d
 * >> C = origin.x^2 + origin.y^2 - F^2
 * >> d = (s - 1) * direction.z
 * >> F = 1 + (s - 1) * origin.z
 */
std::vector<HitRecord> TaperedCylinder::intersect(const Ray& ray) {
    std::vector<HitRecord> hits;
    Ray genRay;
    genRay.origin =  matrixNode.inverse * ray.origin;
    genRay.direction =  matrixNode.inverse * ray.direction;
    float cx = genRay.direction.x;
    float cy = genRay.direction.y;
    float cz = genRay.direction.z;
    float sx = genRay.origin.x;
    float sy = genRay.origin.y;
    float sz = genRay.origin.z;
    float d = (smallRadius - 1.0f) * cz;
    float F = 1.0f + (smallRadius - 1.0f) * sz;
    float A = cx * cx + cy * cy - d * d;
    float B = sx * cx + sy * cy - F * d;
    float C = sx * sx + sy * sy - F * F;
    float discrim = B * B - A * C;
    // Test the wall
    if (discrim > 0.0f) {
        float discRoot = std::sqrt(discrim);
        float t1 = (-B - discRoot) / A; // first hit
        float zHit = genRay.origin.z + genRay.direction.z * t1; // hitpoint.z
        if (t1 > EPSILON && zHit <= 1.0f && zHit >= 0.0f) {
            HitRecord record;
            record.time = t1;
            record.surface = 0; // the wall
            hits.push_back(record);
        }
        float t2 = (-B + discRoot) / A; // second hit
        zHit = genRay.origin.z + genRay.direction.z * t2;
        if (t2 > EPSILON && zHit <= 1.0f && zHit >= 0.0f) {
            HitRecord record;
            record.time = t2;
            record.surface = 0; // the wall
            hits.push_back(record);
        }
    }
    // Test the base at z = 0
    float tb = -genRay.origin.z / genRay.direction.z;
    float squareHitX = std::pow(genRay.origin.x + genRay.direction.x * tb, 2);
    float squareHitY = std::pow(genRay.origin.y + genRay.direction.y * tb, 2);
    // Check if within disc of base
    if (tb > EPSILON && squareHitX + squareHitY < 1.0f) {
        HitRecord record;
        record.time = tb;
        record.surface = 1; // the base
        hits.push_back(record);
    }
    // Test the cap at z = 1;
    float tc = (1.0f - genRay.origin.z) / genRay.direction.z;
    squareHitX = std::pow(genRay.origin.x + genRay.direction.x * tc, 2);
    squareHitY = std::pow(genRay.origin.y + genRay.direction.y * tc, 2);
    // Check if within disc of base
    if (tc > EPSILON && squareHitX + squareHitY < smallRadius * smallRadius) {
        HitRecord record;
        record.time = tc;
        record.surface = 2; // the cap
        hits.push_back(record);
    }
    if (hits.empty())
        return hits; // missed everything
    if (hits.size() == 1) {
        // eye is inside cylinder, only have one exiting hit
        hits[0].entering = false;
        hits[0].object = this;
    } else {
        // Two hits, we must sort them by time
        if (hits[0].time > hits[1].time) {
            float tempTime = hits[0].time;
            int tempSurface = hits[0].surface;
            hits[0].time = hits[1].time;
            hits[0].surface = hits[1].surface;
            hits[1].time = tempTime;
            hits[1].surface = tempSurface;
        }
        // First is entering and second is exiting
        hits[0].entering = true;
        hits[0].object = this;
        hits[1].entering = false;
        hits[1].object = this;
    }
    // Now we set the hit point and normal for each hit record
    for (unsigned int i = 0; i < hits.size(); i++) {
        Vector p = genRay.position(hits[i].time);
        hits[i].point = p;
        // Set normal based on surface type
        if (hits[i].surface == 0) { // wall
            hits[i].surfaceNormal.set(p.x, p.y, (1 - smallRadius) * smallRadius * p.z, 0);
        } else if (hits[i].surface == 1) { // base
            hits[i].surfaceNormal.set(0, 0, -1, 0);
        } else { // cap
            hits[i].surfaceNormal.set(0, 0, 1, 0);
        }

    }

    return hits;
}