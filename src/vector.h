#ifndef VECTOR_H
#define VECTOR_H

#include <iostream>

class Vector {
public:
    float x, y, z, w;
    Vector() : x(0), y(0), z(0), w(0) {}
    Vector(float ax, float ay, float az, float aw = 0) :
        x(ax), y(ay), z(az), w(aw) {}
    void set(float ax, float ay, float az, float aw) {
        x = ax;
        y = ay;
        z = az;
        w = aw;
    }
    Vector operator+(const Vector& other) const {
        return Vector(x + other.x, y + other.y, z + other.z, w + other.w);
    }
    Vector operator-(const Vector& other) const {
        return *this + -other;
    }
    Vector operator-() const {
        return *this * -1;
    }
    friend Vector operator*(const Vector& v, float k);
    friend Vector operator*(float k, const Vector& v);
    friend Vector operator/(const Vector& v, float k);
    friend Vector operator/(float k, const Vector& v);

    float magnitude() const {
        return std::sqrt(x * x + y * y + z * z);
    }

    float dot(const Vector& other) const {
        return x * other.x + y * other.y + z * other.z;
    }

    Vector cross(const Vector& other) const {
        Vector result;
        result.x = y * other.z - z * other.y;
        result.y = z * other.x - x * other.z;
        result.z = x * other.y - y * other.x;
        return result;
    }

    float angle(const Vector& other) const {
        return dot(other)/(magnitude() * other.magnitude());
    }
};

inline Vector operator*(const Vector& v, float k) {
    return Vector(v.x * k, v.y * k, v.z * k, v.w);
}
inline Vector operator*(float k, const Vector& v) {
    return v * k;
}
inline Vector operator/(const Vector& v, float k) {
    return v * (1.0f / k);
}
inline Vector operator/(float k, const Vector& v) {
    return v / k;
}

inline Vector normalize(const Vector& v) {
    return v / v.magnitude();
}

inline std::ostream& operator <<(std::ostream& o, const Vector& v)
{
    o << "( " << v.x << ", " << v.y << ", " << v.z << ", " << v.w << ")";
    return o;
}

#endif